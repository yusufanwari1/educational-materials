1st Berlin Conference on Data Science Education
===============================================

~~*2019-12-03*~~

~~*HTW Berlin, Wilhelminenhofstr. 75A, 12459 Berlin, Germany*~~

**Update:** Unfortunately we had to cancel the conference. Accepted submissions will appear in a regular volume of the [Berlin Journal of Data Science](http://data-science-berlin.de/)

-----------------------------------------------------------

Data Science is an important topic in the curriculum of many modern Computer Science programs.  Still, many students consider Data Science to be hard, since it is heavily based on mathematics and abstract concepts and therefore relatively complicated in comparison to other topics of Computer Science.

The purpose of the *Berlin Conference on Data Science Education* is to provide a forum to present and discuss ideas, concepts, techniques, materials, and experiences of teaching Data Science topics in tertiary education.

We invite high-quality submissions on all aspects of Data Science 
education, including but not limited to:

* Theory, method and practice of Data Science teaching
* Teaching materials
* Experience reports of Data Science teaching
* Lessons learned from student Data Science projects
* Hard- and software infrastructure for research and teaching

We welcome both English and German contributions

Submission
==========

We invite interested authors to submit an extended abstract (up to 4 pages) by 2019-08-31. The abstract will be peer-reviewed, and some of the authors will be invited to the conference. A full paper is to be presented at the conference in 30 minutes. The papers will be published in a special issue of the Berlin Journal of Data Science. The final papers should have 8--12 pages using [this template](http://data-science-berlin.de/article.tex).

The papers will be published under the Creative Commons CC-BY-NC-SA 4.0 License. The special issue of the journal will have an ISBN number.

Submissions must not have been published previously or be currently under consideration for publication elsewhere.

**Submission to:** [submission@data-science-berlin.de](mailto:submission@data-science-berlin.de)

**Conference fee:** None

Publication
===========

All accepted papers will be published under the [Creative Commons CC-BY-NC-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/) in a special volume of [Berlin Journal of Data Science](http://data-science-berlin.de/).

Important Dates
===============

* Submission of abstract:   2019-09-15
* Notification:             2019-10-07
* Submission of Paper:      2019-10-31
* Camera ready deadline:    2019-11-30
* ~~Conference:               2019-12-03~~

<!--
Committee
=========

TBD
-->
