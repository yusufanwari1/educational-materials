---
hideToc: true
---

## Announcements:

* July 2019: Call for abstract has started for the **1st Berlin Conference on Data Science Education**. For more information click [here](conference.md)
* April 2019: Workshop on **23th** of **Mai** **2019**. For more information click [here](workshop.md)

## Purpose of this Website

The goal of [deep.TEACHING](https://www.htw-berlin.de/forschung/online-forschungskatalog/projekte/projekt/?eid=2481) is to improve the qualification of students at HTW Berlin - University of Applied Sciences, partnering companies, as well as external users and students in the machine learning domain. The project aims at providing suitable teaching materials for bachelors and masters programs to impart a theoretical foundation and practical experience.

The [project website](https://www.deep-teaching.org/) provides statically rendered HTML sites to preview the educational materials. In order to interact with notebooks and to run code, clone the [educational-materials](https://gitlab.com/deep.TEACHING/educational-materials) Git repository to your own computer (see [How to use deep.TEACHING Notebooks](#how-to-use-deepteaching-notebooks) for detailed instructions).



## Teaching Material

All teaching material is contextually grouped in one or more scenarios and / or courses and can be found at [Scenarios & Courses](scenarios-courses.md).

All code samples and exercises are written in Python, the most commonly used programming language in this domain. Code and documentation are embedded in [Jupyter](http://jupyter.org/) notebooks to provide an interactive and explorative environment. Jupyter notebooks store source code and markdown formatted documentation in executable *cells*. This approach allows us to create a narrative by splitting up complex algorithms into small, digestable pieces. We recommend [JupyterLab](http://jupyterlab.readthedocs.io/en/stable/getting_started/overview.html), a web-based data science IDE, to work with Jupyter notebooks.

### Scenarios

We are developing three real-world scenarios, Medical Image Classification, Robotic / Autonomous Driving and Text Information Extraction, to focus on relevant educational materials.

The following three pages provide a compact introduction to each of the scenarios. Get started with these introductions and choose a topic you are interested in.

* [Medical Image Classification](courses/medical-image-classification.md)
* [Robotic Autonomous Driving](courses/robotic-autonomous-driving.md)
* [Natural Language Processing](courses/natural-language-processing.md)



### Courses

A course is defined as a set of notebooks, which have something in common, e.g:

* The notebooks in one course develope your understanding of more complex algorithms step-by-step (e.g. from linear regression to logistic regression and then to deep neural networks).
* The notebooks in one course might be about a certain algorithm (e.g. logistic regression) and how to implement it using different librarys (e.g. numpy, tensorflow, pytorch, pymc).
* The notebooks in one course are about different algorithms, but all are to be implemented using the same library.



### Solutions

Some of the provided notebooks contain exercises without any solutions. If you are a teacher, you can request access to the solutions in a [private repository](https://gitlab.com/deep.TEACHING/educational-materials-solutions).

### Feedback for Self-Study

Although the sample solutions will only be accessable for teachers after request, we try to either include software tests, which test your final solution value with the true solution, or include pictures how the visualization of your results should look like (e.g. decreasing error while training or final decision boundary of a classifier). In the case of software tests your solution will be rounded and hashed and then compared with the corrct solution, so you cannot unintendedly spoil your self when looking at the software test. The following code snipped illustrates the hashing process:

```python  
### Exercise, Your Implementation
your_result = your_implemented_function(x,y)

### If your Implementation is correct, the assert below must not throw an exception
assert round_and_hash(your_result) == '52ca17a7de673a7e78903f6a8ea91a0c'
```


### Erros, Misspellings and Bugs

If you find any mistakes, also misspelled words, we would be very grateful if you could open an issue [here](https://gitlab.com/deep.TEACHING/educational-materials/issues).



## Questions?

If you have questions, please open a [GitLab issue](https://gitlab.com/deep.TEACHING/educational-materials/issues).



## Reviews

All notebooks will go through a two-tier review process. The first stage is called *Minor Review* and is usually carried out by a research assistant in the deep.TEACHING team. The secong stage is a *Major Review*, carried out by a more experienced machine learning expert or professor. The review state of each notebook can be inspected in the extensive [notebooks review list](./review-list.md) and a detailed description of the two-tier process can be found in the [Review Guide](review-guide.md).


## How to use deep.TEACHING Notebooks

This tutorial explains how to use the Jupyter notebook teaching materials on your own computer. We recommend using Python 3.6 on a Linux distribution like Fedora 27 or Ubuntu 18.04.


### System Packages

First install `python3`, `python3-pip`, `git` and `graphviz` on your computer.

On **Fedora** run:

```bash
sudo dnf install python3-pip git graphviz
```

On **Ubuntu** run:

```bash
sudo apt update
sudo apt install python3-pip git graphviz
```

If the software repositories of your prefered Linux distribution does not provide Python 3.6 consider using [pyenv](https://github.com/pyenv/pyenv). We are using [pipenv](https://github.com/pypa/pipenv) for dependency resolution, which will make use of `pyenv` automatically, if installed properly.

If you are on **Windows** follow the [Windows Setup](windows-setup.md) instructions.


### Python Packages

After installing the system packages, use the following commands to get up and running.

```bash
# Install pipenv and jupyterlab in user's home directory
pip3 install --user pipenv jupyterlab
# Clone repository via git
git clone https://gitlab.com/deep.TEACHING/educational-materials.git
cd educational-materials
# Create a new virtual environment and install dependencies from Pipfile.lock
pipenv install
# Create an ipython kernel for the virtual environment
pipenv run ipython kernel install --user --name deep_teaching_kernel
# Run Jupyter Lab to navigate through materials
jupyter lab
# When opening a notebook with Jupyter Lab, select deep_teaching_kernel (upper right corner)
```

Using `pipenv` (and `pyenv`) ensures a reproducible setup across platforms and protects us from breaking changes in our Python
dependencies.


## Developer Documentation

* [Developer Guide](developer-guide.md)
* Python Module: [Deep Teaching Commons](https://gitlab.com/deep.TEACHING/deep-teaching-commons)


### Contributing

We would like to build a community of contributors around this project, to collect and improve teaching materials in the machine learning domain. Pull requests are accepted via https://gitlab.com/deep.TEACHING. Please take a look at the [Developer Guide](developer-guide.md) to ensure a consistent quality of the materials.


## Licensing

All materials collected in this repository are meant to be easily accessible and can be used, shared and edited by everyone.

**Notebooks**: Each Juypter notebook contains an individual *License* header to honor the authors of the corresponding notebook. All notebooks are distributed under a CC-BY-SA 4.0 license. This applies to an entire notebook, including code cells, but excluding any external media (e.g. images).

**Code**: Code cells included in the notebooks are dual-licensed as CC-BY-SA 4.0 (as stated in the *Notebooks* section) and the MIT license. The MIT license provides an easy way to reuse code in other software projects, where a Creative Commons license would not be suitable.

**Media**: The media directory contains subdirectories named after each content creator. A LICENSE.md is included in each of these subdirectories, to individually honor content creators by name.
