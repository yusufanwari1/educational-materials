var searchData=
[
  ['t',['T',['../classdp_1_1_node.html#a873abd0d519a285ec5c681898f67d87a',1,'dp::Node']]],
  ['tanh',['tanh',['../classdp_1_1_node.html#a07b9f11486ac0a64885395dbf8d28096',1,'dp::Node']]],
  ['test_5fbranching',['test_branching',['../classtest__dp_1_1_autodiff_test.html#aac95e036835766c0e56d1390fdd79176',1,'test_dp::AutodiffTest']]],
  ['test_5fbroadcasting',['test_broadcasting',['../classtest__dp_1_1_autodiff_test.html#a2596a04d0719dca0a747462a64ce7ff1',1,'test_dp::AutodiffTest']]],
  ['test_5fdp',['test_dp',['../namespacetest__dp.html',1,'']]],
  ['test_5fdp_2epy',['test_dp.py',['../test__dp_8py.html',1,'']]],
  ['test_5finvalid_5foperations',['test_invalid_operations',['../classtest__dp_1_1_autodiff_test.html#a20cd9ec154bad874f2c9f7b1a66501ed',1,'test_dp::AutodiffTest']]],
  ['test_5fmul_5fscalar',['test_mul_scalar',['../classtest__dp_1_1_autodiff_test.html#ada9e167ba3adae4914c1a41d483a7101',1,'test_dp::AutodiffTest']]],
  ['test_5funknown_5finitializer',['test_unknown_initializer',['../classtest__dp_1_1_model_test.html#a6e034727a307dce10def5b88b239bea0',1,'test_dp::ModelTest']]],
  ['train',['train',['../classdp_1_1_optimizer.html#aa4a9f3166f3c2ff59cec886fef8760d3',1,'dp.Optimizer.train()'],['../classdp_1_1_s_g_d.html#a896b271026c7351570acb767b45b2b8d',1,'dp.SGD.train()'],['../classdp_1_1_s_g_d___momentum.html#a0240c22c7f9e6eaf6539067290014c6c',1,'dp.SGD_Momentum.train()'],['../classdp_1_1_r_m_s___prop.html#a5075ea8db217cf4d1201fc7719848e43',1,'dp.RMS_Prop.train()'],['../classdp_1_1_adam.html#a9138bee2fcc87f1d03f9f3af62b19ed6',1,'dp.Adam.train()']]],
  ['transpose',['transpose',['../classdp_1_1_node.html#a25ae29e93554bd11b250ab87486da452',1,'dp::Node']]]
];
