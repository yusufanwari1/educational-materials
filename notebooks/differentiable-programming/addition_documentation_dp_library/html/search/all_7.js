var searchData=
[
  ['get_5fgrad',['get_grad',['../classdp_1_1_model.html#a1009007083e5bca049408348e29454bf',1,'dp::Model']]],
  ['get_5fname_5fand_5fset_5fparam',['get_name_and_set_param',['../classdp_1_1_neural_node.html#a42a6965dc38957678232faf4e06fcbc1',1,'dp::NeuralNode']]],
  ['get_5fparam',['get_param',['../classdp_1_1_node.html#a6da0667fcc3fc1e12d28083d7dfb4293',1,'dp.Node.get_param()'],['../classdp_1_1_model.html#a7b556467894e0e4420ea15cf56aa7c88',1,'dp.Model.get_param()']]],
  ['grad',['grad',['../classdp_1_1_node.html#a0393266d9234a1a6e33cdbb4c31a3e92',1,'dp::Node']]],
  ['grad_5fstores',['grad_stores',['../classdp_1_1_optimizer.html#ab6c2de8b302be8f77b2ddff68f0992d4',1,'dp::Optimizer']]]
];
