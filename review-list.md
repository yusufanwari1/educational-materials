# Notebooks

This is a list of all available Jupyter Notebooks and their review status.

## Machine Learning Fundamentals

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-simple-linear-regression.ipynb</td>
        <td>Christian Herta, Benjamin Voigt</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/16">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-multivariate-linear-regression.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/34">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-logistic-regression.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/40">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-bias-variance-tradeoff.ipynb></td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/41">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-decision-trees.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/65">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-images-data-augmentation-numpy.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/69">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>linear-regression.pdf</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/17">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>linear-regression-exercise.ipynb</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/18">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>logistic-regression.pdf</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/19">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>logistic-regression-exercise.ipynb</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/20">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>evaluation.pdf</a></td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/21">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>evaluation-exercise.ipynb</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/22">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-evaluation-metrics.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/71">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-linear-regression-map.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/36">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-univariate-gaussian-likelihood.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/38">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
</table>




## Math

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>bayes-theorem.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/8">Christoph Jansen</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-bayes-rule.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/23">Benjamin Voigt</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-cookie-problem.ipynb</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/5">Benjamin Voigt</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-matrix-vector-operations.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/32">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-expected-value.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/24">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-univariate-gaussian-basics.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/37">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-multivariate-gaussian.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/35">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-maximum-likelihood-estimator.ipynb</td>
        <td>Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/59">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-column-space-null-space.ipynb</td>
        <td>Christian Herta</td>
        <td><a href=" https://gitlab.com/deep.TEACHING/educational-materials/issues/95">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-jensen-inequality.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href=" https://gitlab.com/deep.TEACHING/educational-materials/issues/100">unassigned</a></td>
        <td>unassigned</td>
    </tr>
</table>

## Scientific Python

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>theory-numpy-and-arrays.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/60">Diyar Oktay, Klaus Strohmenger</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-indexing-and-slicing.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/61">Diyar Oktay</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-fancy-indexing.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/62">Diyar Oktay</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-shaping-and-merging.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/63">Diyar Oktay</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-braodcasting.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/64">Diyar Oktay</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-python-numpy-basics.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/65">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
</table>



## Medical Image Classification

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>data-handling-usage-guide.ipynb</td>
        <td>Neville Augusten, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/85">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>create-custom-dataset.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/86">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-train-cnn-tensorflow.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/87">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-prediction-and-heatmap-generation.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-extract-features.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-classify-heatmaps.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-calculate-kappa-score.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>

</table>



## Natural Language Processing

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-mutual-information.ipynb</td>
        <td>Christian Herta, Diyar Oktay, Klaus Strohmenger</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-naive-bayes.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/57">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
</table>




## Uncategorized

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>blueprint.ipynb</td>
        <td>Benjamin Voigt, Christoph Jansen</td>
        <td>not applicable</td>
    </tr>
    <tr>
        <td>package_test_workshop_2019_05.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td>not applicable</td>
    </tr>
</table>




## Differentiable Programming

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr
    <tr>
        <td>exercise-automatic-differentiation-scalar.ipynb</td>
        <td>Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/66">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise_automatic-differentiation-matrix.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/67">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-automatic-differentiation-neural-network.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/70">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-weight-initialization.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/79">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-optimizers.ipynb</td>
        <td>Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/80">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-batch-norm.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/81">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-dropout.ipynb</td>
        <td>Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/82">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-activation-functions.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/56">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pytorch-univariate-linear-regression.ipynb</td>
        <td>Christian Herta,  Klaus Strohmenger, Benjamin Voigt</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/72">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pytorch-multivariate-linear-regression.ipynb</td>
        <td>Christian Herta,  Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/73">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pytorch-softmax-regression.ipynb</td>
        <td>Christian Herta,  Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/74">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pytorch-logistic-regression.ipynb</td>
        <td>Christian Herta,  Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/77">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pytorch-simple-neural-network.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/78">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-tensorflow-softmax-regression.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/31">unassigned</a></td>
        <td>unassigned</td>
    </tr>
</table>




## Graphical Models

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-EM-simple-example.ipynb</td>
        <td>Christian Herta, Klaus Strohmger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/43">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-d-separation.ipynb</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/9">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-bayesian-networks-by-example.ipynb</td>
        <td>Christoph Jansen</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/6">Benjamin Voigt</a></td>
        <td>unassigned</td>
    </tr>    
    <tr>
        <td>exercise-forward-reasoning-probability-tables.ipynb</td>
        <td>Christian Herta, Klaus Strohmger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/39">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-1d-gmm-em.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-2D-gaussian-mixture-em.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/99">unassigned</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-hierarchical-model-for-univariate-gaussian-data.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
</table>




## Information Theory

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-entropy.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/26">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-kullback-leibler-divergence.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/42">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
</table>




## Monte Carlo Simulation

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-biased-monte-carlo-estimator.ipynb</td>
        <td>Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/88">Oliver Fischer</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variance-sample-size-dependence.ipynb</td>
        <td>Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/89">Oliver Fischer</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-sampling-inverse-transform.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/25">unassigned</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-sampling-importance.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-sampling-rejection.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-MCMC-Metropolis-sampling.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/101">unassigned</a</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-sampling-crude-monte-carlo-estimator.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variance-reduction-by-control-variates.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variance-reduction-by-importance-sampling.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variance-reduction-by-reparametrization.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variance-reduction-via-rao-blackwellization.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
</table>




## Probabilistic Programming

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-pymc3-examples.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/83">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pymc3-bundesliga-predictor.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/84">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pymc3-ranking.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/90">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pymc3-topic-models-latent-dirichlet-allocation.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pyro-simple-gaussian.ipynb</td>
        <td>Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/92">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pyro-bundesliga-predictor.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/92">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pyro-bayesian-polynominal-regression.ipynb</td>
        <td>Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/94">Klaus Strohmenger</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-linear-regression-polynominal-tensorflow-probability.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
</table>

## Reinforcement Learning

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-10-armed-bandits-testbed.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/91">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-gridworld.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/96">unassigned</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-monte-carlo-frozenlake-gym.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/97">unassigned</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-TD-q-learning-frozenlake-gym.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/98">unassigned</a></td>
        <td>unassigned</td>
    </tr>
     <tr>
        <td>markov-decision-processes-and-optimal-control.ipynb</td>
        <td>Oliver Fischer</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/102">unassigned</a></td>
        <td>unassigned</td>
    </tr>
   
</table>




## Sequence Learning

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-bi-gram-language-model.ipynb</td>
        <td>Christoph Jansen, Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/15">Christian Herta, Klaus Strohmenger</a></td></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-hidden-markov-models.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/46">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-memm.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/47">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-sensorfusion-and-kalman-filter-1d.ipynb</td>
        <td>Christian Herta</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/58">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-pytorch-char-rnn-reber-grammar.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-linear-chain-crf.ipynb</td>
        <td>Christian Herta, Diyar Oktay</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/48">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
</table>





## Variational

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-bayesian-by-backprop.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variational-autoencoder.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variational-EM-bayesian-linear-regression.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-variational-mean-field-approximation-for-a-simple-gaussian.ipynb</td>
        <td>Christian Herta</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
</table>




## Feed Forward Networks

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-image-classification-softmax-regression.ipynb</td>
        <td>Benjamin Voigt, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/44">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-backprop.ipynb</td>
        <td>Benjamin Voigt, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/45">Klaus Strohmenger</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-nn-pen-and-paper.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href='https://gitlab.com/deep.TEACHING/educational-materials/issues/52'>Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-simple-neural-network.ipynb</td>
        <td>Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/50">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-nn-rework.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/55">Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-nn-framework.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href='https://gitlab.com/deep.TEACHING/educational-materials/issues/51'>Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-convolution.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href='https://gitlab.com/deep.TEACHING/educational-materials/issues/51'>Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-conv-net-pen-and-paper.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href='https://gitlab.com/deep.TEACHING/educational-materials/issues/75'>Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-cnn-framework.ipynb</td>
        <td>Benjamin Voigt</td>
        <td><a href='https://gitlab.com/deep.TEACHING/educational-materials/issues/54'>Diyar Oktay</a></td>
        <td>unassigned</td>
    </tr>

</table>




## Unsorted

<table>
    <tr>
        <th>Notebook</th>
        <th>Authors</th>
        <th>Minor Review</th>
        <th>Major Review</th>
    </tr>
    <tr>
        <td>exercise-meaning-of-softmax-output-probability.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td><a href="https://gitlab.com/deep.TEACHING/educational-materials/issues/33">unassigned</a></td>
        <td>unassigned</td>
    </tr>
    <tr>
        <td>exercise-natural-pairing.ipynb</td>
        <td>Christian Herta, Klaus Strohmenger</td>
        <td>unassigned</td>
        <td>unassigned</td>
    </tr>
    
</table>
